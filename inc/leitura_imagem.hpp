#ifndef LEITURA_IMAGEM_HPP
#define LEITURA_IMAGEM_HPP
#include<string>
#include<fstream>
using namespace std;

class Leitura_Imagem{

private:
int altura;
int largura;
int tamanho;
int pixel_inicio1;

string tipo_de_imagem;
string comentario;
string pixel_inicio;
string nome_do_arquivo;

protected:
char * pixels;
public:

Leitura_Imagem();
~Leitura_Imagem();

void setAltura(int altura);
int getAltura();

void setLargura(int largura);
int getLargura();

void setTamanho(int tamanho);
int getTamanho();

void setPixel_Inicio1(int pixel_inicio1);
int getPixel_Inicio1();

void setTipo_deImagem(string tipo_de_imagem);
string getTipo_deImagem();

void setComentario(string comentario);
string getComentario();

void setPixel_Inicio(string pixel_inicio);
string getPixel_Inicio();

void setNome_do_Arquivo(string nome_do_arquivo);
string getNome_do_Arquivo();

void setPixels(char *pixels);
char * getPixels();

void Lendo_Imagem();

};

#endif
